# Manifeste personnel (par [jibe-b](https://gitlab.com/jibe-b))

## Polyactivité

À titre personnel, j'envisage la polyactivité en lieu et place de la monoactivité majoritaire. Cela exige de ma part de clarifier mes compétences et mes souhaits d'activité domaine par domaine, ainsi que de démarcher de potentiels clients dans chacun de ces domaines.

La période d'apprentissage a commencé il y a plus de 10 ans pour certains domaines, quand elle a débuté il y a quelques mois pour d'autres.

## Contributions à l'échelle individuelle (activité) et collective (coopérative)

La construction d'activité multi-facettes est possible au sein de coopératives polyactivités. Ma participation à une CAE me permet d'être indépendant mais il est évidemment plus favorable d'être à plusieurs en partenariat.

À grande échelle, ma participation se fait en développant une CAE polyactivité alliant les activités de recherche et les activités agricoles.

## Pratiques de recherche scientifique

Bien que le cadre académique soit le cadre le plus connu, les pratiques de recherche scientifique ont leur place également dans le cadre privé. La qualité des tentatives de production de savoirs et modèles dépend de la qualité technique et mathématique des expériences et analyses réalisées.

Il est à noter que toute personne, tant qu'elle fait les efforts nécessaires (en s'associant avec d'autres personnes autant que nécessaire), est en mesure d'exercer des activités de recherche de qualité.

## Problématiques scientifiques en lien avec des questions sociétales

En ces temps de questionnement sur les effets du changement climatique sur l'environnement, les mesures des milieux agricoles participent à la collecte de données stratégiques.

Par ailleurs, de nombreux membres de la société civile expérimentent des pratiques agricoles variées. La réalisation de ces expériences au sein de nombreuses exploitations pourra permettre d'évaluer leur intérêt au regard de différents milieux.

