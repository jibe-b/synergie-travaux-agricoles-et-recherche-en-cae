# Questions portant sur les CAE en général

[FAQ CAE](https://gitlab.com/open-scientist/my-own-coop/blob/master/FAQ.md)

# Questions portant sur la gouvernance liquide et la forkabilité

[FAQ CAE-gouvernance-liquide](https://gitlab.com/jibe-b/cae-gouvernance-liquide/blob/master/FAQ.md)

[FAQ cae-forkabilité](https://gitlab.com/jibe-b/cae-forkable/blob/master/FAQ.md)


# Questions portant sur la CAE comme cadre pour la polyactivité

[FAQ CAE-polyactivité](https://gitlab.com/jibe-b/cae-comme-cadre-pour-la-polyactivite/blob/master/FAQ.md)

# Quel est le rôle de la CAE en termes de coordination des activités agricoles et des activités de recherche

La CAE fait office d'intermédiaire pour le paiement des travailleu·se·r·s agricoles à temps partiel,

tout comme elle fait office d'intermédiaire pour les activités de recherche exercées par les exploitant·e·s.

# Y a-t-il une place pour les nouvelles installations en sur une ferme

Oui. La CAE ne résoud pas la problématique de la terre. En revanche, si la terre est disponible, la CAE peut être un proxy pour sa location.

# Y a-t-il une place pour les exploitant·e·s installé·e·s

Oui. Les exploitant·e·s installé·e·s n'ont pas besoin de la CAE pour leur contrat de travail agricole. En revanche, pour la rémunération des activités de recherceh, un contrat de travail entre la CAE et les travailleu·se·r·s agricoles est signé.

# Un·e travailleu·se·r doit-iels se consacrer uniquement à ces deux activités

Non. En termes d'organisation les membres de la CAE fixeront un taux de participation aux activités de la CAE, afin d'assurer sa pérennité, mais le temps restant peut être alloué à d'autres activités (photographe-clown-guide-de-haute-montagne par exemple).


